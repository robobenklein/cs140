# cs140
Lab code from UTK's CS140 of Fall 2017.

If you're here to copy-paste answers you've come to the wrong place.

If you're here for the makefile and python scripts, by all means please take that code, I found it useful perhaps you can too.

The selfupdate sources for the makefile and canvas script are on github gists tho, you can find the link to that in any of the makefiles.
