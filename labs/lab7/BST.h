#ifndef BST_H
#define BST_H

#include <iomanip>
#include <iostream>
#include <queue>
#include <vector>
using namespace std;

template <class TKey>
class bst {
  struct node {

	void print();

    friend ostream& operator<<(
        ostream& os,
        const bst<TKey>::node t_node
    ) {
        // to also show node IDs:
        // os << "node[" << t_node.ID << "](" << t_node.key << ")";
        os << t_node.key;
        return os;
    }

    TKey key;
	// ID parameter
    int ID;

	// parent info
    node *parent;
    // left, right
    node *link[2];

    node(int n_ID = 0) {
        ID = n_ID;
        parent = nullptr;
        key = TKey();
    }
  };

  public:
    class iterator {
      public:
        //   default constructor (no argument)
        iterator() { p = nullptr; }
        // overloaded operators (++, *, ==, !=)
        bool operator==(const iterator other);
        bool operator!=(const iterator other);
        node operator*(){ return *p; }
        node* operator->(){ return p; }
        iterator& operator++();
      private:
        friend class bst<TKey>;
        // constructor (with argument)
        iterator(node *n_p) {
            p = n_p;
        }
        node *p;
    };

    iterator begin() {
        node *start_n = Troot;

        while (start_n->link[0] != nullptr ) {
            start_n = start_n->link[0];
        }
        // new_it = iterator();
        return iterator(start_n);
    }
    iterator end() {
        return iterator();
    }

  public:
    bst() {
        Troot = nullptr;
    }
	~bst() { clear(Troot); }

	bool empty() {
        return Troot==NULL;
    }

	void insert(TKey &);

    iterator lower_bound(const TKey &);
	iterator upper_bound(const TKey &);

	void print_bylevel();

  private:
	void clear(node *);
	node *insert(node *, TKey &);

	// ID parameter support
    int n_elements = 0;
    node *Troot;
};

template <class TKey>
void bst<TKey>::node::print()
{
    cout << "ID[" << setw(3) << ID << "]:(" << setw(3) << key << ");";

    // output node and parent ID information
    // change below to output subtree ID information

    if (parent) {
        cout << " P=ID[" << setw(3) << parent->ID << "]:" << "(" << setw(3) << parent->key << ")";
    }
    else {
        // wishing I could just print(" " * N)
        cout << "                ";
    }
    if (link[0]) {
        cout << " L=ID[" << setw(3) << link[0]->ID << "]:" << "(" << setw(3) << link[0]->key << ")";
    }
    else {
        cout << "                ";
    }
    if (link[1]) {
        cout << " R=ID[" << setw(3) << link[1]->ID << "]:" << "(" << setw(3) << link[1]->key << ")";
    }
    else {
        cout << "                ";
    }

    cout << "\n";
}

// bst<TKey>::iterator functions not defined above go here

// operator==
template <class TKey>
bool bst<TKey>::iterator::operator==(const bst<TKey>::iterator other) {
    return p == other.p;
}


// operator!=
template <class TKey>
bool bst<TKey>::iterator::operator!=(const bst<TKey>::iterator other) {
    return p != other.p;
}

// operator++
template <class TKey>
typename bst<TKey>::iterator& bst<TKey>::iterator::operator++() {
    // if the right subtree doesn't exist
    // we go up to the first greater parent
    if (p == nullptr) {
        // are we raising exceptions in this class?
        // would do it here if so
        // otherwise, can't increment any more
        return *this;
    }
    else if (p->link[1] == nullptr) {
        // cout << "going up to parent\n";
        while (p->parent) {
            if (p->parent->key > p->key) {
                p = p->parent;
                return *this;
            } else {
                p = p->parent;
            }
        }
        // if we got out here, we got the end (null)
        // cout << "yo got to the end nullptr\n";
        p = p->parent;
        return *this;
    }
    // if there's something to the right
    else {
        // cout << "going right then leftmost\n";
        p = p->link[1];
        // follow the leftmost branch down
        while (p->link[0]) {
            p = p->link[0];
        }
        return *this;
    }
}

template <class TKey>
void bst<TKey>::clear(node *T)
{
  if (T) {
    clear(T->link[0]);
    clear(T->link[1]);
    delete T;
    T = NULL;
  }
}

template <class TKey>
void bst<TKey>::insert(TKey &key)
{
  Troot = insert(Troot, key);
}

template <class TKey>
class bst<TKey>::node *bst<TKey>::insert(node *n_T, TKey &key)
{
    if (n_T == nullptr) {
        // update and set node ID
        n_T = new node(n_elements);
        n_elements++;
        n_T->key = key;
        n_T->link[0] = nullptr;
        n_T->link[1] = nullptr;
    } else if (n_T->key == key) {
        // it's already been set?
    } else {
        int direction = (n_T->key) < key;
        n_T->link[direction] = insert(n_T->link[direction], key);
        // set parent link
        n_T->link[direction]->parent = n_T;
    }

    return n_T;
}

// bst<TKey>::lower_bound function goes here
template <class TKey>
typename bst<TKey>::iterator bst<TKey>::lower_bound(const TKey& searchkey) {
    node *search_node = Troot;
    node *best_match = nullptr;

    while (search_node != nullptr) {
        if (search_node->key < searchkey) {
            // search right
            search_node = search_node->link[1];
            continue;
        }
        else { // search_node->key >= searchkey
            best_match = search_node;
            // search on the left
            search_node = search_node->link[0];
        }

    }

    iterator searching_it;
    searching_it = iterator(best_match);
    return searching_it;
}

// bst<TKey>::upper_bound function goes here
template <class TKey>
typename bst<TKey>::iterator bst<TKey>::upper_bound(const TKey& searchkey) {
    node *search_node = Troot;
    node *best_match = nullptr;

    while (search_node != nullptr) {
        if (search_node->key <= searchkey) {
            // search right
            search_node = search_node->link[1];
            continue;
        }
        else { // search_node->key >= searchkey
            best_match = search_node;
            // search on the left
            search_node = search_node->link[0];
        }

    }

    iterator searching_it;
    searching_it = iterator(best_match);
    return searching_it;
}

template <class TKey>
void bst<TKey>::print_bylevel()
{
  if (Troot == NULL)
    return;

  queue<node *> Q;
  node *T;

  Q.push(Troot);
  while (!Q.empty()) {
    T = Q.front();
    Q.pop();

    T->print();
    if (T->link[0]) Q.push(T->link[0]);
    if (T->link[1]) Q.push(T->link[1]);
  }
}

#endif // BST_H
