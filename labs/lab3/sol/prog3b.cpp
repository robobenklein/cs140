#include <cstdlib>
#include <iostream>
#include <iomanip>

#include <algorithm>
#include <list>

using namespace std;

bool verbose = false;

class isprime {
  private:

  public:
    list<int> primes;
    //isprime();
    //~isprime();

    // raw math to check primality
    bool primality_test(const int inputint) {
        if (verbose) cout << "Checking number: " << inputint << endl;

        if (inputint <= 1) return false;
        else if (inputint <= 3) return true;
        else if (inputint % 2 == 0 || inputint % 3 == 0) return false;
        else {
            // go through M to sqrt(N)
            int i = 5;
            while (i * i <= inputint) {
                if (inputint % i == 0) return false;
                else if (inputint % (i+2) == 0) return false;
                // cheap trick to increment 6 as the lcm of 2 and 3
                i = i + 6;
            }
        }
        return true;
    }
    // true if number is in primes list
    bool prime_in_cache(const int inputint) {
        return std::find(primes.begin(), primes.end(), inputint) != primes.end();
    }
    // caches new primes while testing them
    bool primality_check_autocache(const int inputint) {
        if (prime_in_cache(inputint)) {
            if (verbose) cout << "Number is prime (cached): " << inputint << endl;
            return true;
        }
        else {
            bool is_a_prime = primality_test(inputint);
            if (is_a_prime) {
                if (verbose) cout << "Caching new prime: " << inputint << endl;
                primes.push_back(inputint);
                return true;
            }
            else return false;
        }
    }

    // default action when instance is called as function
    bool operator()(const int inputint) {
        return primality_check_autocache(inputint);
    }

};

// operator for list<int>
ostream& operator<< (ostream& outstream, list<int>& intlist) {
    outstream << "list<int>[";
    for (list<int>::iterator iT = intlist.begin(); iT != intlist.end(); ++iT) {
        outstream << *iT << ",";
    }
    outstream << "]";
}

// operator for isprime class
ostream& operator<< (ostream& outstream, isprime& primecheckclass) {
    outstream << "isprime(){primes: " << primecheckclass.primes << "}";
}

int main(int argc, char *argv[])
{
    for (int i=1; i<argc; i++) {
      string option = argv[i];
      if (option.compare("-verbose") == 0) {
        verbose = true;
      } else 
        cout << "option " << argv[i] << " ignored\n";
    }

    
    cerr << "Is it prime? [Input an integer]" << endl;

    isprime primechk = isprime();

    int input_number;
    while (cin >> input_number) {
        if (primechk(input_number)) cout << "yep" << endl;
        else { cout << "nope" << endl; }

        // utility functions for debugging
        switch (input_number) {
            // -2: show isprime instance
            case -2: cout << primechk << endl; break;
        }
    }

}

