
#include <iomanip>
#include <iostream>
#include <vector>

using namespace std;

class stats {
  private:
    int totalcount = 0,
        min, max, sum = 0;
    vector<int> intvect;


  public:
    stats();
    ~stats();
    void push(int incoming);
    void print();
};

stats::stats() {};
stats::~stats() {};

void stats::push(int incoming) {
    intvect.push_back(incoming);

    if (totalcount == 0) {
        min = incoming;
        max = incoming;
    }
    
    totalcount += 1;
    sum += incoming;

    if (incoming > max) {
        max = incoming;
    }
    if (incoming < min) {
        min = incoming;
    }
}

void stats::print() {
    cout << "N   = " << totalcount << endl;
    cout << "sum = " << sum << endl;
    cout << "min = " << min << endl;
    cout << "max = " << max << endl;
}

int main() {
    
    int incoming_int;
    stats mystats = stats();

    while (cin >> incoming_int) {
        mystats.push(incoming_int);
    }
    
    mystats.print();
}

