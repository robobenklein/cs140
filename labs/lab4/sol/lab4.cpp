#include <sstream>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <vector>
#include <string>
#include <algorithm>

using namespace std;
typedef unsigned int uint;

class hash_table {
  public:
    struct key_line {
        string key;
        vector<int> lnums;
        bool inuse();
        bool operator==(key_line&);
    };
    hash_table();
    void insert(const string&, int);
    const vector<int> find(string&);
  private:
    int hash(const string&);
    int nextprime(int);
    int qprobe(const string&);
    void resize();
    int num_inuse;
    int max_inuse;
    vector<key_line> table;
};

hash_table::hash_table() {
    int N = 23;
    table.assign(N, key_line());
    num_inuse = 0;
    max_inuse = N/2; // quadratic probe max value
}

bool hash_table::key_line::inuse() {
  if (key=="") return false;
  else return true;
}

int hash_table::hash(const string &word) {
  int index = 0;
  const char *c = word.c_str();
  while (*c)
    index = ((index << 5) | (index >> 27)) + *c++;
  return (uint)index % table.size();
}

void hash_table::insert(const string &word, int line_num) {
    int index = qprobe(word);
    table[index].key = word;
    table[index].lnums.push_back(line_num);
    if (++num_inuse >= max_inuse) {
        resize();
    }
}

// return a vector of line numbers
const vector<int> hash_table::find(string &word) {
  for (int i=0;i<table.size();i++) {
    if (table[i].key==word) return table[i].lnums;
  }
  vector<int> empty;
  return empty;
}

int hash_table::nextprime(int N) {
    int i = 2;
    while (i*i <= N) {
        if (N%i == 0) { N++; i = 1; }
        i++;
    }
    return max(3,N);
}

int hash_table::qprobe(const string &word) {
  int index = hash(word);
  int k = 0;
  while (table[index].inuse() && table[index].key != word) {
    index += 2*(++k)-1;
    index = index % table.size();
  }
  return index;
}

void hash_table::resize() {
  vector<key_line> tmp_table;
  for (int i=0; i<(int)table.size(); i++) {
    if (table[i].inuse())
      tmp_table.push_back(table[i]);
  }
  int N = nextprime(2*table.size());
  table.assign(N, key_line());
  num_inuse = 0;
  max_inuse = N/2;
  for (int i=0; i<(int)tmp_table.size(); i++) {
    key_line temp = tmp_table[i];
    table[ qprobe(temp.key) ] = temp;
    num_inuse++;
  }
}

int main(int argc, char *argv[]) {
    if (argc<2) {
        cout << "Fewer than two arguments used.\nUsage: ./hash_table file_example.txt" << endl;
        return 1;
    }

    hash_table H;
    string key;
    ifstream fin;
    string curline, tempword;
    vector<string> filecache;
    int line_number = 0;

    fin.open(argv[1]);

    if (fin.fail()) {
        cerr << "could not open file for reading" << endl;
        return 2;
    }

    while (getline(fin, curline)) {
        filecache.push_back(curline);

        // cout << setw(3) << line_number+1 << " " << curline << endl;

        for (string::iterator i = curline.begin(); i != curline.end(); ++i) {
            if (ispunct(*i)) *i = ' ';
        }
        stringstream ss(curline);
        while(ss >> tempword) {
            // place word, line, in hash_table
            H.insert(tempword, line_number);
        }
        ++line_number;
    }
    // done with file
    fin.close();

    // debugging: show file cache and line numbers
    // for (int i = 0; i < filecache.size(); ++i) {
    //     cout << setw(3) << i+1 << ": " << filecache[i] << endl;
    // }

    vector<int> printlines;

    cout << "find> ";
    while(cin >> tempword) {
        printlines = H.find(tempword);
        // display lines in order
        sort( printlines.begin(), printlines.end() );
        // eliminate duplicate lines
        printlines.erase(
            unique( printlines.begin(), printlines.end() ),
            printlines.end()
            );
        // print the lines with numbers
        for (int i=0; i < printlines.size(); ++i) {
            cout << setw(3) << printlines[i]+1 << ": " << filecache[printlines[i]] << endl;
        }
        cout << "find> ";
    }

  return 0;
}
