Problem 1
[10]

T R U P Q N S

Diagrams drawn where '|' indicates the left branch, '\' indicated the right/greater branch.

[0] Sketch the binary search tree produced for the above data where each letter is a key.

T
|\
R U
|\
P S
|\
N Q

[3] Delete node R by promoting its inorder predecessor. Sketch the resulting binary tree.

T
|\
Q U
|\
P S
|
N

[3] Delete node Q by promoting its inorder predecessor. Sketch the resulting binary tree.

T
|\
P U
|\
N S

[2] Delete node N by promoting its inorder predecessor. Sketch the resulting binary tree.

T
|\
P U
 \
  S

[2] Delete node T by promoting its inorder predecessor. Sketch the resulting binary tree.

S
|\
P U

