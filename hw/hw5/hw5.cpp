
#include <iomanip>
#include <iostream>
#include <vector>

// try int, double, float, string, etc...
#define test_datatype string

using namespace std;

template <class T>
class stats {
  private:
    int totalcount = 0;
    T sum = T();
    vector<T> objects;


  public:
    stats();
    ~stats();
    void push(T incoming);
    void print();
};

template <class T>
stats<T>::stats() {};
template <class T>
stats<T>::~stats() {};

template <class T>
void stats<T>::push(T incoming) {
    objects.push_back(incoming);
    
    totalcount += 1;
    sum += incoming;

}

template <class T>
void stats<T>::print() {
    cout << "N   = " << totalcount << endl;
    cout << "sum = " << sum << endl;
}

int main() {
    
    test_datatype incoming_item;
    stats<test_datatype> mystats = stats<test_datatype>();

    while (cin >> incoming_item) {
        mystats.push(incoming_item);
    }
    
    mystats.print();
}

