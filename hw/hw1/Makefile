
# CS140 makefile
# original by robobenklein / bklein3@utk
# GPLv3 license
# contact me if this breaks for some reason,
# but it's not my fault if you delete your programs using this

CC:=g++

mkfile_path := $(abspath $(lastword $(MAKEFILE_LIST)))
current_dir := $(notdir $(patsubst %/,%,$(dir $(mkfile_path))))

dirname = $(patsubst %/,%,$(dir $1))

lab:=$(notdir $(current_dir))
tardir:=$(abspath $(dir $(mkfile_path)))
tarfile:=$(tardir)/$(lab)_$(shell date --rfc-3339=date).tar.gz
SOURCEDIR := .

CPP_FILES := $(shell find $(SOURCEDIR) -name '*.cpp')
PROGS = $(patsubst %.cpp,%,$(CPP_FILES))

CC_FLAGS := -std=c++11

all: $(PROGS)
	# sources: $(CPP_FILES)
	# outputs: $(PROGS)

%: %.cpp
	# building: $@
	$(CC) $(CC_FLAGS)  -o $@ $<

tar:
	# lab: $(lab)
	tar -cvzf $(TAR_FLAGS) $(tarfile) $(CPP_FILES)

update:
	which curl
	$(eval updatefile = /tmp/${USER}.update.makefile )
	curl -fsSL https://gist.githubusercontent.com/robobenklein/9c67cfebed431003aa11cabbbf4438a3/raw/Makefile > $(updatefile)
	test -s $(updatefile)
	gvfs-trash $(mkfile_path)
	mv $(updatefile) $(mkfile_path)
	test -s $(mkfile_path)
	# if the updated version doesn't work, the old Makefile is in your trash, probably ~/.local/share/Trash/
	# if there's a problem with the latest version, report bugs to bklein3@vols.utk.edu
	# thanks for using my Makefile!

help:
	@echo -e '\033[36mCS140 makefile\033[0m by \033[31mrobobenklein\033[0m aka \033[36mbklein3\033[0m'
	@echo -e 'Running \033[36mmake\033[0m will compile these files: '
	@echo -e '    \033[44m$(CPP_FILES)\033[0m'
	@echo -e 'into these output programs: '
	@echo -e '    \033[44m$(PROGS)\033[0m'
	@echo -e 'only if the source files have changed, so no need to recompile irrelevant parts'
	@echo -e ''
	@echo -e 'When done with a lab, you can use `make tar` to tar.gz your .cpp files'
	@echo -e 'which will put all of these files: '
	@echo -e '    \033[44m$(CPP_FILES)\033[0m'
	@echo -e 'into a tar.gz file here: \033[36m$(tarfile)\033[0m'
	@echo -e ''
	@echo -e 'If you want to know what a make command will do before you run it,'
	@echo -e ' use \033[36mmake -n\033[0m in place of \033[36mmake\033[0m and make will show you the commands without running them'
	@echo -e 'Note that things beginning with a \033[36m#\033[0m are comments!'
	@echo -e ''
	@echo -e 'If you have feedback, bugs, or suggestions, email me! <\033[36mbklein3@vols.utk.edu\033[0m>'
